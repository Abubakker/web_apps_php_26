<?php
namespace EmailApp\BITM\SEIP120340\Email;
use PDO;
class Email {
    //put your code here
    public $id = '';
    public $unicque_id = '';
    public $varificatdion_id = '';
    public $user_name = '';
    public $pass = '';
    public $email = '';
    public $is_active = '';
    public $is_admin = '';
    public $create = '';
    public $modify = '';
    public $delete = '';
    public $dbuser = 'root';
    public $dbpass='';
    public $conn='';
    
    function __construct()
    {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=web_apps_php_26', $this->dbuser, $this->dbpass);

    }//construct End
    
    public function prepare($data = ''){
        if(array_key_exists('id', $data)){
                $this->id = $data['id'];
        }
        if(array_key_exists('unicque_id', $data)){
                $this->unicque_id = $data['unicque_id'];
        }
        if(array_key_exists('varificatdion_id', $data)){
                $this->varificatdion_id = $data['varificatdion_id'];
        }
        if(array_key_exists('user_name', $data) && !empty($data['user_name']) && (strlen($data['user_name'] < 6)) && (strlen($data['user_name'] > 12))){
                $this->user_name = $data['user_name'];
        } else {
            $_SESSION['User_name_Err']="Please Inser Your User Name Minimum 6 and Maximum 12 Character ";
        }
        if(array_key_exists('pass', $data) && !empty($data['pass']) && ($_POST['password'] === $_POST['confirm'])){
                $this->pass = $data['password'];
        } else {
            $_SESSION['pass_Err']="Please Inser Your Password and Confirm same";
        }
        if(array_key_exists('email', $data) && !empty($data['pass']) && (!filter_var($_POST['email'] , FILTER_VALIDATE_EMAIL) === false)){
                $this->email = $data['email'];
        } else {
            $_SESSION['email_Err'] = $_POST['email'] . " is not a valid email address";
        }
        
        return $this ;
    }// Prepare End
    
    public function store(){
        try {
           $query = "INSERT INTO email (id, unique_id, varification_id, user_name, password, email, is_active, is_admin, created, modified, deleted)
           VALUES (:id, :un_id, :v_id, :u_name, :pass, :email, :is_ac, :is_ad, :crt, :mod , :del)";
           $stmt=  $this->conn->prepare($query);
           $stmt->execute(array(
               ':id' => NULL,
               ':un_id' => uniqid(),
               ':v_id' => uniqid(),
               ':u_name' => $this->user_name,
               ':pass' => $this->pass,
               ':email' => $this->email,
               ':is_ac' => 0,
               ':is_ad' => 0,
               ':crt' => date("Y/m/d"),
               ':mod' => date("Y/m/d"),
               ':del' => date("Y/m/d"),
           ));
//              header('location:index.php');
           }
       catch(PDOException $e){
            echo $sql . "<br>" . $e->getMessage();
           }


    } // Store End
                           
}
