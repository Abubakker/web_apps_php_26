<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" type="text/css" href="CSS/stylesheet.css">
                <link rel="stylesheet" type="text/css" href="../../../../Bootstrap/css/bootstrap.css">

		<!-- Website CSS style -->
                <link rel="stylesheet" type="text/css" href="../../../../Bootstrap/css/bootstrap.min.css">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Admin</title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Sign UP Form</h1>
                                <h2>Input All Field</h2>
	               		<hr />
                                
	               	</div>
	            </div> 
				<div class="main-login main-center">
                                    <form class="form-horizontal" method="post" action="Store.php">
						
						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="user_name" id="username"  placeholder="Enter your Username"/>                                                                        
								</div>
                                                            <?php
                                                                if(!empty($_SESSION['User_name_Err'])){
                                                                    echo $_SESSION['User_name_Err'];
                                                                    unset($_SESSION['User_name_Err']);
                                                                }
                                                            ?>
							</div>
						</div>	

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
                                                            <?php
                                                                if(!empty($_SESSION['pass_Err'])){
                                                                    echo $_SESSION['pass_Err'];
                                                                    unset($_SESSION['pass_Err']);
                                                                }
                                                            ?>
							</div>
						</div>
                                            
                                                <div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                                        <input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
                                                            <?php
                                                                if(!empty($_SESSION['email_Err'])){
                                                                    echo $_SESSION['email_Err'];
                                                                    unset($_SESSION['email_Err']);
                                                                }
                                                            ?>
							</div>
						</div>
                                            

						<div class="form-group ">
							<input type="submit" value="Register">
						</div>
						<div class="login-register">
				            <a href="index.php">Login</a>
				         </div>
					</form>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	</body>
</html>