<title>Update</title>

<?php 

require_once '../../../../src/bitm/seip120340/mobile/Semister.php';

use SemisterApp\bitm\seip120340\mobile\Semister ;

$objUpdate = new Semister;

$objUpdate -> prepare($_POST) -> update();
