<title>Delete</title>

<?php 

require_once '../../../../src/bitm/seip120340/mobile/Semister.php';

use SemisterApp\bitm\seip120340\mobile\Semister ;

$objDelete = new Semister;

$objDelete -> prepare($_GET) -> delete();