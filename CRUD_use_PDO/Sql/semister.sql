-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 06:04 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `example`
--

-- --------------------------------------------------------

--
-- Table structure for table `semister`
--

CREATE TABLE IF NOT EXISTS `semister` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `semister` varchar(255) NOT NULL,
  `offer` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `waiver` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semister`
--

INSERT INTO `semister` (`id`, `name`, `semister`, `offer`, `cost`, `waiver`, `total`, `unique_id`) VALUES
(26, 'Abu bakker', '1st Semister', 'offered', '8000', '800', '7200', '5790461ae8550'),
(27, 'bnak', '2nd Semister', 'offered', '12000', '1200', '10800', '57904622db495'),
(28, 'bnak', '3rd Semister', 'Not Offered', '16000', '0', '16000', '57904629c2bfb'),
(29, 'dsoifjsadf', '1st Semister', 'Not Offered', '8000', '0', '8000', '579049a461a03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `semister`
--
ALTER TABLE `semister`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `semister`
--
ALTER TABLE `semister`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
