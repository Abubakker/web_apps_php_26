<?php
?>

<a href="../../../../Index.php">View all project</a><br>
<a href="Create.php">Add a new Mobile Model</a>
<a href="View.php">View Mobile Model</a>
<a href="Edit.php">Add a new Mobile Model</a>



<?php
include_once '../../../../Source/BITM/SEIP120340/Mobile/Mobile.php';

$obj = new Mobile();
$Alldata = $obj->index();
?>

<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th>Unique Id</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    

    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['title'] ?></td>
                <td><?php echo $Singledata['unique_id'] ?></td>
                <td><a href="View.php?id=<?php echo $Singledata['id'] ?>">View</a></td>
                <td><a href="Edit.php?id=<?php echo $Singledata['id'] ?>">Edit</a></td>
                <td><a href="Delete.php?id=<?php echo $Singledata['id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
