<?php
include_once "../../../../Source/BITM/SEIP120340/Mobile/Mobile.php";

$obj = new Mobile();

$onedata = $obj->prepare($_GET)->view();

?>
<a href="Index.php">Back to list</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Unique ID</th>
    </tr>
    <tr>
        <td><?php echo $onedata['id'] ?></td>
        <td><?php echo $onedata['title'] ?></td>
        <td><?php echo $onedata['unique_id'] ?></td>
    </tr>
</table>