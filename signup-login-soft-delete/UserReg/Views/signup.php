<?php
include_once("../vendor/autoload.php");
use App\users\users;
$obj = new users();
$data = $obj->index();
//echo "<pre>";
//print_r($data);

//if (!empty($_SESSION['Message'])) {
//    echo $_SESSION['Message'];
//    unset($_SESSION['Message']);
//}
include("include/header.php");
?>

<!-- Registration form - START -->
<div class="container">
    <div class="row">
        <form role="form" method="post" action="signup-process.php">
            <div class="col-lg-6">
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required
                        Field</strong></div>
                <div class="form-group">
                    <label for="InputName">Username*</label>

                    <div class="input-group">
                        <input type="text" name="uname" class="form-control" name="InputName" id="InputName"
                               placeholder="Username" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="InputName">Password*</label>

                    <div class="input-group">
                        <input type="password" name="pw1" class="form-control" name="InputName" id="InputName"
                               placeholder="Password" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="InputName">Confirm Password*</label>

                    <div class="input-group">
                        <input type="password" name="pw2" class="form-control" name="InputName" id="InputName"
                               placeholder="Confirm password" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Enter Email</label>

                    <div class="input-group">
                        <input type="email" name="email" class="form-control" id="InputEmailFirst" name="InputEmail"
                               placeholder="Enter Email" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>

                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
            </div>
        </form>
        <div class="col-lg-5 col-md-push-1">
            <div class="col-md-12">
                <?php if (isset($_SESSION['Message'])) { ?>
                    <div class="alert alert-success">
                        <strong></span>
                            <?php
                            echo $_SESSION['Message'];
                            unset($_SESSION['Message']);
                            ?>
                        </strong>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<!-- Registration form - END -->

</div>

</body>
</html>