<?php


?>
<html>
    <head>
        <title>Profile</title>        
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

    </body>
</html>


    
<div class="container text-center custom" id="ourform">
    <h2><small>Welcome our Contact Form! Fill up your information for contact</small></h2>
    <br /><br />
    <form action="" method="post">
        <table bgcolor="#00FFCC">
            <tr>
                <th><label for="name">First Name : </label></th>
                <td><input type="text" class="form-control" name="first_name" size="30" placeholder="Your First Name"/></td>
            </tr>
            <tr>
                <th><label for="name">Last Name : </label></th>
                <td><input type="text" class="form-control" name="last_name" size="30" placeholder="Your Last Name"/></td>
            </tr>
            <tr>
                <th><label for="name">Father Name : </label></th>
                <td><input type="text" class="form-control" name="father_name" size="30" placeholder="Your Father Name"/></td>
            </tr>
            <tr>
                <th><label for="name">Mother Name : </label></th>
                <td><input type="text" class="form-control" name="mother_name" size="30" placeholder="Your Mother Name"/></td>
            </tr>                      
            <tr>
                <th><label for="name">Village : </label></th>
                <td><input type="text" class="form-control" name="village" size="30" placeholder="Your Village Name"></td>
             </tr>
             <tr>
                <th><label for="name">Post : </label></th>
                <td><input type="text" class="form-control" name="post" size="30" placeholder="Your Post Name"></td>
             </tr><tr>
                <th><label for="name">Thana : </label></th>
                <td><input type="text" class="form-control" name="thana" size="30" placeholder="Your Thana Name"></td>
             </tr>
             <tr>
                <th><label for="name">District : </label></th>
                <td><input type="text" class="form-control" name="district" size="30" placeholder="Your District Name"></td>
             </tr>
             <tr>
                <th><label for="name">Division : </label></th>
                <td><input type="text" class="form-control" name="division" size="30" placeholder="Your Division Name"></td>
             </tr>
             <tr>
                <th><label for="name">Mobile Number : </label></th>
                <td><input type="text" class="form-control" name="mobile_number" size="30" placeholder="+8801"></td>
             </tr>
             <tr>
                <th><label for="name">Gender : </label></th>
                <td>
                    <input type="radio" name="gender" value="mail"> Mail
                    <input type="radio" name="gender" value="femail"> Femail
                    <input type="radio" name="gender" value="others"> Others
                </td>
             </tr>                 
             <tr>
                <th><label for="name">Location : </label></th>
                <td>
                    <SELECT name="location" size="1">
                        <OPTION value="dhaka">Dhaka</OPTION>
                        <OPTION value="rajshahi">Rajshahi</OPTION>
                        <OPTION value="khulna">Khulna</OPTION>
                        <OPTION value="chittagong">Chittagong</OPTION>
                        <option value="barisal">Barisal</option>
                        <option value="sylhet">Sylhet</option>
                        <option value="rangpur">Rangpur</option>
                    </SELECT>
                    </td>
             </tr>
             <tr>
                <th><label for="name">Date of Birth : </label></th>
                <td><input type="date" class="form-control" name="birth_day" size="30"></td>
             </tr>
             <tr>
                <th><label for="name">Email : </label></th>
                <td><input type="email" class="form-control" name="email" size="30" placeholder="example@gmail.com"></td>
             </tr>
             <tr>
                <th><label for="name">User name : </label></th>
                <td><input type="text" class="form-control" name="user_name" size="30" placeholder="Your User Name"></td>
             </tr>
             <tr>
                <th><label for="name">Create a password : </label></th>
                <td><input type="password" class="form-control" name="password" size="30"></td>
             </tr>
             <tr>
                <th><label for="name">Confirm your password : </label></th>
                <td><input type="password" class="form-control" name="confirm_password" size="30"></td>
             </tr>
             <tr>
                <th><label for="name">Position : </label></th>
                <td>
                    <input type="radio" name="div_year" value="morning"> First Year
                    <input type="radio" name="div_year" value="after_noon"> Second Year
                    <input type="radio" name="div_year" value="night"> Third Year
                    </td>
             </tr>
             <tr>
                <th><label for="name">Language : </label></th>
                <td>
                    Bangla : <input type="checkbox" name="bangla" value="bangla">
                    English : <input type="checkbox" name="english" value="english">
                    Arbian : <input type="checkbox" name="arbian" value="arbian">
                    Hinde : <input type="checkbox" name="hinde" value="hinde">
                    </td>
             </tr>
             <tr>
                <th><label for="name">Comment : </label></th>
                <td><textarea rows="5" cols="40" class="form-control" name="comment"></textarea></td>
             </tr>
             <tr>
                    <th>&nbsp;</th>
                <td>&nbsp;</td>
             </tr> 
             <tr>
                    <th></th>
                <td>
                    <button type="reset" class="btn btn-primary">Reset </button>
                    <button type="submit" class="btn btn-success">Save</button>
                </td>
             </tr>                 
          </table>
        </form>
</div>