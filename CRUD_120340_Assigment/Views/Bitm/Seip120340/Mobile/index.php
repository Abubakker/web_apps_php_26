<?php
include_once '../../../../vendor/autoload.php';

 use MobileApp\Bitm\Seip120340\Mobile\Mobile;
 
?>
<a href="../../../../index.php">List of Project</a><br>
<a href="create.php">Add New Model</a><br>
<span id="utility">Download Data Base as <a href="pdf.php">PDF File</a></span><br>
<span id="utility01">Download Data Base as <a href="xl.php">Excel File</a></span><br>
<?php
$obj = new Mobile();
$Alldata = $obj->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th>Unique Id</th>
        <th>Laptop</th>
        <th colspan="3">Action</th>
    </tr>
    <?php

    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['Mobile'] ?></td>
                <td><?php echo $Singledata['unique_id'] ?></td>
                <td><?php echo $Singledata['laptop'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['unique_id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['unique_id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['unique_id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
