<?php
include ('../../../../vendor/mpdf/mpdf/mpdf.php');
include_once ('../../../../vendor/autoload.php');
use MobileApp\Bitm\Seip120340\Mobile\Mobile;

$obj=new Mobile();
$allData=$obj->index();
$trs = "";
$serial=0;
foreach ($allData as $data):
    $serial++;
$trs.="<tr>";
$trs.="<td>".$serial."</td>";
$trs.="<td>".$data['id']."</td>";
$trs.="<td>".$data['Mobile']."</td>";
$trs.="<td>".$data['laptop']."</td>";
$trs.="<td>".$data['unique_id']."</td>";
$trs.="</tr>";
endforeach;

$html = <<<EOD
<html>
    <head>
        <title>List of Mobile Model</title>
    </head>
    <body>
        <h1>List Mobile model </h1>
        <table border="1">
            <thead>
                <tr>
                    <th>SL.</th>
                    <th>ID</th>
                    <th>Mobile Model</th>
                    <th>Laptop Model</th>
                    <th>Unique ID</th>
                </tr>
            </thead>
            <tbody>
                $trs;
            </tbody>
        </table>

    </body>
</html>
        
        
EOD;

$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>