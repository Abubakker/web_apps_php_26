<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors',TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI == 'cli')
    die('This example should only be run from a Browser');

// Include PHPExel

require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../../../vendor/autoload.php';

use MobileApp\Bitm\Seip120340\Mobile\Mobile;
$objExcel = new Mobile();
$Alldata = $objExcel ->index();

// Create new phpExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel ->getProperties()->setCreator("Maarten Balliauw")
                        ->setLastModifiedBy("Maarten Balliauw")
                         ->setTitle("Office 2007 XLSX Test Document")
                         ->setSubject("Office 2007 XLSX Test Document")
                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                         ->setKeywords("Office 2007 openxml php")
                         ->setCategory("Test result file");

//Add some Data

$objPHPExcel ->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'SL')
                         ->setCellValue('B1', 'ID')
                         ->setCellValue('C1', 'Mobile Model')
                         ->setCellValue('D1', 'Laptop Model')
                         ->setCellValue('E1', 'Unique ID');

$counter = 2;
$serial = 0;

foreach($Alldata as $data){
    $serial++;
    $objPHPExcel ->setActiveSheetIndex(0)
                ->setCellValue('A'.$counter, $serial)
                ->setCellValue('B'.$counter, $data['id'])
                ->setCellValue('C'.$counter, $data['Mobile'])
                ->setCellValue('D'.$counter, $data['laptop'])
                ->setCellValue('E'.$counter, $data['unique_id']);
                $counter++;
}

// Rename Worksheet

$objPHPExcel->getActiveSheet()->setTitle('Mobile_list');

// Set Active sheet index to the first sheet, so Excel  open this as the first sheet
$objPHPExcel ->setActiveSheetIndex(0);

// Redirect output to a clients web broser
header('countent-type: application/vnd.ms-excel');
header('Content-disposition: attachment;filename= "01simple.xls"');
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter -> save('php://output');
exit;