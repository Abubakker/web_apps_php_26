<?php
include_once '../../../../Source/BITM/SEIP120340/Mobile/Mobile.php';
$objEdit = new Mobile();
//if (isset($_GET['id']) && !empty($_GET['id'])){
//    $single = $objEdit->prepare($_GET)->show();
//}
$single = $objEdit->prepare($_GET)->show();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//print_r($single);
//die();
?>

<html>
    <head>
        <title>
            Edit
        </title>
    </head>
    <body>
        <a href="Index.php">Back to List</a>
        <fieldset>
            <legend>
                Update Mobile Model
            </legend>
            <form action="Update.php" method="post">
                <label>Update Mobile Model</label>
                <input type="text" name="mobile_model" value="<?php echo $single["mobile_model"] ?>">
                <input type="hidden" name="id" value="<?php echo $_GET["id"] ?>">
                <input type="submit" value="Update Mobile Model">
            </form>
        </fieldset>
    </body>
</html>