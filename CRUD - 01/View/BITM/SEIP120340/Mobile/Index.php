<?php
include_once "../../../../Source/BITM/SEIP120340/Mobile/Mobile.php";
?>

<a href="../../../../Index.php">View My All Project</a><br>
<a href="Create.php">Add a new Mobile Model</a><br><br><br>

<?php
$objIndex = new Mobile();
$Alldata=$objIndex->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

//echo "<pre>";
//print_r($data);
?>

<html>
    <head>
        <title>Index | Data</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>Serial Number</th>
                <th>Mobile Model</th>
                <th colspan="3">Action</th>
            </tr>
<?php
$serial = 1;
if (isset($Alldata) && !empty($Alldata)){

    foreach ($Alldata as $Singledata){ ?>
    <tr>
        <td><?php echo $serial++ ?></td>
        <td><?php echo $Singledata['mobile_model'] ?></td>
        <td><a href="Show.php?id=<?php echo $Singledata["id"]; ?>">View</a></td>
        <td><a href="Edit.php?id=<?php echo $Singledata["id"]; ?>">Edit</a></td>
        <td><a href="Delete.php?id=<?php echo $Singledata["id"]; ?>">Delete</a></td>
    </tr>                
<?php } } else { ?>
    <tr>
        <td colspan="3">No Available Data in Database</td>       
    </tr> 
     
<?php } ?>            
    
        </table>
    </body>
</html>
