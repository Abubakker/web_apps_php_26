<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;

$objLoginaction = new Registration();
?>

<html lang="en">
<head>
  <title>home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <style> p {padding: 5px;border-radius: 5px;font-size: 15px;}</style>
</head>
<body>

    <div class="container" style="margin-top:5%;">
        <div class="row">
            <?php
            
            if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {
                ?>
            
<div class="container">
	<div class="row">
                <div class="col-md-6 col-xs-12 center-block" align="center">
                    <div class="panel panel-primary">
                    <div class="panel-heading">
                            <h3 class="panel-title">Welcome <strong><?php echo $_SESSION['user']['username'];?></strong> to your profile</h3>
                    </div>
                    <div class="panel-body center-block"> 
                        <img src="http://lorempixel.com/output/people-q-c-100-100-1.jpg" class="center-block img-circle"/><br>
                        <p class="bg-primary">Your Username <?php echo $_SESSION['user']['username'];?></p>
                        <p class="bg-primary">Your email <?php echo $_SESSION['user']['email'];?></p>
                        <p class="bg-primary">Profile created time <?php echo $_SESSION['user']['created'];?></p>
                        <p class="bg-primary"><a class="btn btn-primary btn-block" href="profile.php" role="button"><b>Click to See Full Profile Details</b></a></p>
                        
                        <?php
                            if( $_SESSION['user']['is_admin'] ==1){
                                ?>
                        
                        <div class="btn-group btn-group-justified" role="group">
                            
                            <a class="btn btn-default" role="button" href="edit.php?id=<?php echo $_SESSION['user']['unique_id'];?>">Edit</a>
                            <a class="btn btn-default" role="button" href="delete.php?id=<?php echo $_SESSION['user']['unique_id'];?>">Delete</a>
                            <a class="btn btn-default" role="button" href="photo.php?id=<?php echo $_SESSION['user']['unique_id'];?>">Upload Photo</a>
                            <a class="btn btn-default" role="button" href="list.php">User list</a>
                            
                            
                        </div><!--btn group-->
                        
                        <?php
                        
                            }else{
                                ?>
                                <a class="btn btn-default" role="button" href="photo.php?id=<?php echo $_SESSION['user']['unique_id'];?>">Upload Profile Pic</a>
                                <?php
                            }
                        
                        ?>
                        

                    </div><!--panel-body-->
                    
            
        </div>
                    <a href="logout.php"><button type="button" class="btn btn-default btn-lg">
                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Logout</button></a>
    </div> 
                <?php

                }else{
                    echo '<a href="login.php" class="btn btn-info btn-lg btn-block">Sorry you are not logged yet. Click here for login</a>';    
                }
    ?>
      
            <?php include 'footermenu.php'; ?>