<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';
use RegApp\Bitm\Seip10\registration\Registration;


$objEdit = new Registration();

$userData = $objEdit -> singleUser();

if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Registration</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <style>.autowidth {display:inline-block !important;width:auto !important;</style>
</head>
 
 <body>
      <div class="container" style="margin-top:5%;margin-bottom:5%">
         <div class="row">
                 <!-- left column -->
    <div class="update_pic col-md-5">
        <div class="text-center">
            <img src="../img/user1.jpeg" class="avatar img-circle img-thumbnail" alt="avatar">
            <h6>Upload a different photo...</h6>
            <input type="file" class="text-center center-block well well-sm">
        </div>
    </div><!-- update_pic -->
                 
    <!-- edit form column -->
    <div class="update_info col-md-7">
      <h2>Update Personal info</h2>
      <br>
     
            <form class="form-horizontal" role="form" method="POST" action="update.php">

              <label>First name:</label>
              <input class="form-control" name="full_name" value="<?php echo $userData['full_name']; ?>" type="text"><br>

              <label>Father name:</label>
              <input class="form-control" name = "father_name" value="<?php echo $userData['father_name']; ?>" type="text"><br>

              <label>Mother name:</label>
              <input class="form-control" name = "father_name" value="<?php echo $userData['mother_name']; ?>" type="text"><br>

              <label>Update your birthday :</label>
                 <div>
                     <select name="day">
                          <?php
                              for($i=1; $i <=31; $i++){
                                  echo "<option value=".$i.">".$i."</option>";
                              }
                          ?>
                      </select>
                      <!--input month-->
                      <select name="month"> 
                          <option value="January">January</option>
                          <option value="February">February</option>
                          <option value="March">March</option>
                          <option value="April">April</option>
                          <option value="May">May</option>
                          <option value="June">June</option>
                          <option value="July">July</option>
                          <option value="September">September</option>
                          <option value="October">October</option>
                          <option value="November">November</option>
                          <option value="December">December</option>    
                      </select>
                      <!--input year-->
                      <select name="year">
                          <?php
                          for($i =1976;$i <= 2014; $i++){

                              echo "<option value = ".$i.">".$i."</option>";
                          }
                          ?>
                      </select>
                 </div><br>

              <label>Your Gender : </label>
              <div>
                  <input type="radio" name="gender" value="male" <?php // echo ($$userData['gender'] == 'male'?'checked':''); ?>>  Male
                  <input type="radio" name="gender" value="female" <?php // echo ($$userData['gender'] == 'female'?'checked':''); ?>>  Female
              </div><br>

              <label>Occupation:</label>
              <input class="form-control" name = "occupation" value="<?php echo $userData['occupation']; ?>" type="text"><br>
              
              <label>Education:</label>
              <input class="form-control" name = "education" value="<?php echo $userData['education']; ?>" type="text"><br>
              
              <label>Religion:</label>
              <input class="form-control" name = "religion" value="<?php echo $userData['religion']; ?>" type="text"><br>
              
              <label>Marritial Status:</label>
              <input class="form-control" name = "marritial_stat" value="<?php echo $userData['marritial_stat']; ?>" type="text"><br>
              
              <label>Job Status:</label>
              <input class="form-control" name = "job_status" value="<?php echo $userData['job_status']; ?>" type="text"><br>
              
              <label>Nationality :</label>
              <input class="form-control" name = "Nationality" value="<?php echo $userData['Nationality']; ?>" type="text"><br>
              
              <label>Interest :</label><br>
              <input type="checkbox" name = "interest[]" value="Reading"> Reading Book  <input type="checkbox" name = "interest[]" value="Coding"> Coding & music  <input type="checkbox" name = "interest[]" value="Cycling"> Cycling  <br><br>
              
              <label>Bio:</label>
              <textarea class="form-control" name="bio"><?php echo $userData['bio']; ?></textarea>
              
              <label>Mobile Number :</label>
              <input class="form-control" name = "mobile" value="<?php echo $userData['mobile']; ?>" type="number"><br>
              
              <label>National ID Number :</label>
              <input class="form-control" name = "nid" value="<?php echo $userData['nid']; ?>" type="number"><br>
              
              <label>PassPort Number :</label>
              <input class="form-control" name = "passpoet" value="<?php echo $userData['passpoet']; ?>" type="number"><br>
              
              <label>Fax Number :</label>
              <input class="form-control" name = "fax" value="<?php echo $userData['fax']; ?>" type="number"><br>
              
              <label>Skills Area :</label>
              <input class="form-control" name = "skill_arel" value="<?php echo $userData['skill_arel']; ?>" type="text"><br>
              
              <label>Languages :</label>
              <input type="checkbox" name = "languages[]" value="Bangla"> Bangla  <input type="checkbox" name = "languages[]" value="English">  English  <input type="checkbox" name = "languages[]" value="Hindi">  Hindi  <br><br>
              
              <label>Blood Group:</label>
              <select name="blood[]"> 
                  <option value="a_pos">A+</option>
                  <option value="b_pos">B+</option>
                  <option value="ab_pos">AB+</option>
                  <option value="o_pos">O+</option>
                  <option value="a_neg">A-</option>
                  <option value="b_neg">B-</option>
                  <option value="ab_neg">AB-</option>
                  <option value="o_neg">O-</option>
              </select><br>
              
              <label>Height :</label>
              <input class="form-control" name = "height" value="<?php echo $userData['height']; ?>" type="text"><br>
              
              <label>Address :</label>
              <input class="form-control" name = "add1" value="<?php echo $userData['address']; ?>" placeholder="Address" type="text"><br>
              
              <input class="form-control autowidth" name = "street" value="<?php echo $userData['address']; ?>" placeholder="Street" type="text">
              <input class="form-control autowidth" name = "area" value="<?php echo $userData['address']; ?>" placeholder="Area/Police Station" type="text">
              <input class="form-control autowidth" name = "city" value="<?php echo $userData['address']; ?>" placeholder="City/Division" type="text">
              <input class="form-control autowidth" name = "zip" value="<?php echo $userData['address']; ?>" placeholder="ZIP Code" type="number"><br><br>
              
              <input class="btn btn-primary" value="Save Changes" type="submit">

        </form>
    </div> <!-- update_info -->
            
<?php include 'footermenu.php'; ?>

<?php
    
}else{
    
    $_SESSION['errorMsg'] = "Sorry ! You don't have the permission to access";
    header("location:error.php");
}
  
        
?>