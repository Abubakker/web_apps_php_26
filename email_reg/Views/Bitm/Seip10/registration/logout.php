<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;

$objlogout = new Registration();

session_unset();
session_destroy();
header("location:login.php");
?>