<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;


$objProfile = new Registration();

$userData = $objProfile -> singleUser();

if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
  <link href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="screen">  
</head>
<body>

    <div class="container" style="margin-top:5%;">
        <div class="row user-menu-container square">
           <div class="col-md-6 user-details">
            <div class="row coralbg white">
                <div class="col-md-6 no-pad">
                    <div class="user-pad">
                        <h3 style="font-family: Nimbus Mono L"><b><?php echo $userData['full_name']; ?></b></h3>
                        <h4 class="white"><i class="fa fa-male"></i> <?php echo $userData['gender']; ?></h4>
                        <h4 class="white"><i class="fa fa-birthday-cake"></i> <?php echo $userData['birthdate']; ?></h4>
                        <h4 class="white"><i class="fa fa-mobile"></i> <?php echo $userData['mobile']; ?></h4>
                        <h4 class="white"><i class="fa fa-map-marker"></i> <?php echo $userData['address']; ?></h4>
                        <a type="button" class="btn btn-success btn-lg"" href="edit.php">
                            <span class="btn-label"><i class="fa fa-pencil"></i></span>Update Profile
                        </a>
                    </div>
                </div>
                <div class="col-md-6 no-pad">
                    <div class="user-image">
                        <img src="../img/user3.jpg" class="img-responsive thumbnail">
                    </div>
                </div>
            </div>
            <div class="row overview">
                <div class="col-md-12 user-pad text-center">
                    <h3>CREATE DATE</h3>
                    <h4><?php echo $_SESSION['user']['created']; ?></h4>
                </div>
            </div>
        </div>
        <div class="col-md-1 user-menu-btns">
            <div class="btn-group-vertical square" id="responsive">
                <a href="#" class="btn btn-block btn-default active">
                    <i class="fa fa-info"></i><p>About</p>
                </a>
                <a href="#" class="btn btn-default">
                  <i class="fa fa-briefcase"></i><p>Academic</p>
                </a>
                <a href="#" class="btn btn-default">
                  <i class="fa fa-user"></i><p>Personal</p>
                </a>
                <a href="#" class="btn btn-default">
                  <i class="fa fa-envelope"></i><p>Contact</p>
                </a>
            </div>
        </div>
        <div class="col-md-4 user-menu user-pad">
            <div class="user-menu-content active">
                <h3>
                    About Myself
                <ul class="user-menu-list">
                    <li>
                        <h4><i class="fa fa-user coral"></i><?php echo $userData['full_name']; ?></h4>
                    </li>
                    <li>
                        <h4>Blood Group : <?php echo $userData['blood']; ?></h4>
                        <h4>Religion : <?php echo $userData['religion']; ?></h4>
                    </li>
                    <li>
                        <h4><i class="fa fa-heart-o coral"></i> Maritial Status : <?php echo $userData['marritial_stat']; ?></h4>
                    </li>
                    <li>
                        <h4><i class="fa fa-flag"></i> Nationality : <?php echo $userData['Nationality']; ?></h4>
                    </li>
                </ul>
            </div>
            <div class="user-menu-content">
                <h3>
                    Your Inbox
                </h3>
                <ul class="user-menu-list">
                    <li>
                        <h4>From Roselyn Smith <small class="coral"><strong>NEW</strong> <i class="fa fa-clock-o"></i> 7:42 A.M.</small></h4>
                    </li>
                    <li>
                        <h4>From Jonathan Hawkins <small class="coral"><i class="fa fa-clock-o"></i> 10:42 A.M.</small></h4>
                    </li>
                    <li>
                        <h4>From Georgia Jennings <small class="coral"><i class="fa fa-clock-o"></i> 10:42 A.M.</small></h4>
                    </li>
                    <li>
                        <button type="button" class="btn btn-labeled btn-danger" href="#">
                            <span class="btn-label"><i class="fa fa-envelope-o"></i></span>View All Messages</button>
                    </li>
                </ul>
            </div>
            <div class="user-menu-content">
                <h3>
                    Trending
                </h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="view">
                            <div class="caption">
                                <p>47LabsDesign</p>
                                <a href="" rel="tooltip" title="Appreciate"><span class="fa fa-heart-o fa-2x"></span></a>
                                <a href="" rel="tooltip" title="View"><span class="fa fa-search fa-2x"></span></a>
                            </div>
                            <img src="http://24.media.tumblr.com/273167b30c7af4437dcf14ed894b0768/tumblr_n5waxesawa1st5lhmo1_1280.jpg" class="img-responsive">
                        </div>
                        <div class="info">
                            <p class="small" style="text-overflow: ellipsis">An Awesome Title</p>
                            <p class="small coral text-right"><i class="fa fa-clock-o"></i> Posted Today | 10:42 A.M.</small>
                        </div>
                        <div class="stats turqbg">
                            <span class="fa fa-heart-o"> <strong>47</strong></span>
                            <span class="fa fa-eye pull-right"> <strong>137</strong></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="view">
                            <div class="caption">
                                <p>47LabsDesign</p>
                                <a href="" rel="tooltip" title="Appreciate"><span class="fa fa-heart-o fa-2x"></span></a>
                                <a href="" rel="tooltip" title="View"><span class="fa fa-search fa-2x"></span></a>
                            </div>
                            <img src="http://24.media.tumblr.com/282fadab7d782edce9debf3872c00ef1/tumblr_n3tswomqPS1st5lhmo1_1280.jpg" class="img-responsive">
                        </div>
                        <div class="info">
                            <p class="small" style="text-overflow: ellipsis">An Awesome Title</p>
                            <p class="small coral text-right"><i class="fa fa-clock-o"></i> Posted Today | 10:42 A.M.</small>
                        </div>
                        <div class="stats turqbg">
                            <span class="fa fa-heart-o"> <strong>47</strong></span>
                            <span class="fa fa-eye pull-right"> <strong>137</strong></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-menu-content">
                <h2 class="text-center">
                    START SHARING
                </h2>
                <center><i class="fa fa-cloud-upload fa-4x"></i></center>
                <div class="share-links">
                    <center><button type="button" class="btn btn-lg btn-labeled btn-success" href="#" style="margin-bottom: 15px;">
                            <span class="btn-label"><i class="fa fa-bell-o"></i></span>A FINISHED PROJECT
                    </button></center>
                    <center><button type="button" class="btn btn-lg btn-labeled btn-warning" href="#">
                            <span class="btn-label"><i class="fa fa-bell-o"></i></span>A WORK IN PROGRESS
                    </button></center>
                </div>
            </div>
        </div>
            
            
            
            
            
            <?php
            
            }else {
                $_SESSION['regNotyet'] = '<p class="text-danger">Sorry, Please login first to see the profile.</p>';
                        header('location:login.php');
            }
            ?>
    
<?php include 'footermenu.php'; ?>

