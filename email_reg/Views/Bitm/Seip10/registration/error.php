<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container" style="margin:5%">
        <div class="row">
            <h1 style="font-size: 62px;font-weight: 700;text-shadow: 2px 2px 4px;">404 Error !</h1>
            <a href="index.php"><h2 class="bg-danger" style="padding:10px;">

                <?php
            if(!empty($_SESSION['errorMsg']) && isset($_SESSION['errorMsg']) ){

                echo $_SESSION['errorMsg'];
                unset($_SESSION['errorMsg']);
                
            }// if \\
        
        ?>
                . To return homepage Click Here</h2></a>
            
            </h2>
