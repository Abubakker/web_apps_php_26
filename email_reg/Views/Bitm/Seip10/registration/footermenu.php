        <?php
            function activeClass($path=""){

               if(basename($_SERVER['REQUEST_URI'],'.php') == $path){
                   return "active";
               }
        }

        ?>
            <nav class="navbar navbar-default navbar-fixed-bottom">
                    <div class="navbar-header">
                        <div id="bs-example-navbar-collapse-7" class=" collapse navbar-collapse">

                            <a class="navbar-brand"><img alt="Brand" src="../css/bitm.png"></a>

                            <ul class="nav navbar-nav">
                                <li class = "<?php echo activeClass('create'); ?>">
                                    <a href="create.php">Register</a>
                                </li>
                                <!--****-->
                                <li class = "<?php echo activeClass('login'); ?>">
                                    <a href="login.php">Login</a>
                                </li>
                                 <!--****-->
                                 <?php
                                 if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {

                                     echo '<li class ="'.activeClass('profile').'"><a href="profile.php">Profile</a></li>';
                                     echo '<li class ="'.activeClass('index').'"><a href="index.php">Single View</a></li>';
                                        if( $_SESSION['user']['is_admin'] ==1){

                                            echo '<li class ="'.activeClass('list').'"><a href="list.php">List</a></li>';
                                        }

                                 } else{

                                     echo '<li class =""><a href="login.php">Loing for Profile</a></li>';

                                 }  

                                    ?>
                            </ul>
                        </div>
                </div>
            </nav>
            
                <?php
                if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {
                ?>
            <nav class="navbar navbar-default navbar-fixed-top">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Logged as <?php echo $_SESSION['user']['username'];?><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="logout.php">Logout</a></li>
                  </ul>
                </li>
              </ul>
            </nav>

              <?php 
                
              }
              
              ?>


            </div><!-- row -->
        </div><!--container --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../js/bootstrap.js" type=""></script>
        <script src="../js/custom.js" type=""></script>
    </body>
</html>
