<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;

$objlist = new Registration();

$data = $objlist ->alluser();

if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {
    
    if( $_SESSION['user']['is_admin'] ==1){
        $i = 1; $serial = 0;

        ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" style="margin-top:5%;">
    <div class="row">
        
       <?php $objlist->warningMsg('listSuccess'); ?>
            
        <div class="panel panel-primary">
           <table class="table table-bordered">
            <div class="panel-heading">
                 <h2 class="center-block" align="center">List of All Users</h2>
                <tr class="info">
                  <th>ID</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Email</th>
                  <th>AdminShip</th>
                  <th>Varified</th>
                  <th>Created</th>
                  <th>Modified</th>
                  <th>Deleted</th>
                  <th colspan="3" style="text-align: center;">Action</th>
                </tr>
            </div>
               <div class="panel-body">
                   
                       <tbody>
                            <?php
                            foreach($data as $singleData){
                                if($singleData['is_deleted'] == 1){
                                    continue;
                                }
                                
                                ?>
                          <tr class="<?php      if($i %2 == 0){
                                                echo "success";}else{
                                                    echo "warning";
                                                }
                                                $i++;  ?>">
                                <td><?php echo $serial++;?></td>
                                <td><?php echo $singleData['username']; ?></td>
                                <td><?php echo $singleData['password']; ?></td>
                                <td><?php echo $singleData['email']; ?></td>
                                <td><?php echo $singleData['is_admin']; ?></td>
                                <td><?php echo $singleData['is_active']; ?></td>
                                <td><?php echo $singleData['created']; ?></td>
                                <td><?php echo $singleData['modified']; ?></td>
                                <td><?php echo $singleData['deleted']; ?></td>
                                <td><a href="profile.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">View</td>
                                <td><a href="edit.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">Edit</td>
                                <td><a href="trash.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">Delete</td>

                          </tr>
                          <?php 
                            }
                            ?>
                    </tbody>
                  </table>
            
</div><a href="trashlist.php">Want to see trash list</a>

<?php include 'footermenu.php'; ?>

<?php
    } else{
        $_SESSION['errorMsg'] = "This page is only for ";
         header("location:error.php");
    }

        

    
}else{
    
    $_SESSION['errorMsg'] = "Sorry ! You don't have the permission to access";
    header("location:error.php");
}
  
        
?>