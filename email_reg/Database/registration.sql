-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 03, 2016 at 08:47 অপরাহ্ণ
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercise`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verified_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `unique_id`, `verified_id`, `username`, `password`, `email`, `is_admin`, `is_active`, `created`, `modified`, `deleted`, `is_deleted`) VALUES
(43, '57a09a8f05668', '57a09a8f0562e', 'Hasan212', 'asdfasdf', 'hasan@two.com', 0, 1, '2016-08-02 03:05:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(45, '57a0a8ab34bb3', '57a0a8ab34b74', 'Ratan58', '321654', 'teis@sesaa.com', 0, 0, '2016-08-02 04:05:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(46, '57a0a93e55af7', '57a0a93e55abb', 'Shan23', 'asdfasdf', 'asdf@tass.com', 0, 1, '2016-08-02 04:07:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(48, '57a0b720399d7', '57a0b7203998e', 'Aslam34', ';lkj;''lkj', 'aslam@mail.com', 0, 1, '2016-08-02 05:07:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(49, '57a0b7968a83c', '57a0b7968a803', 'Ratan58', 'asdfasdf', 'sisir017@yahoo.com', 0, 1, '2016-08-02 05:09:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(60, '57a0c08aabdcb', '57a0c08aabd90', 'mehedi123', 'qwerqwer', 'ratan@gmail.com', 1, 1, '2016-08-02 05:47:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
