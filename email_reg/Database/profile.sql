-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 07, 2016 at 03:51 পূর্বাহ্ণ
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercise`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `marritial_stat` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `job_status` varchar(255) NOT NULL,
  `Nationality` varchar(255) NOT NULL,
  `interest` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `nid` int(11) NOT NULL,
  `passpoet` int(255) NOT NULL,
  `skill_arel` varchar(255) NOT NULL,
  `languages` varchar(255) NOT NULL,
  `profile_pic` longblob NOT NULL,
  `blood` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `full_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `father_name`, `mother_name`, `gender`, `birthdate`, `occupation`, `education`, `religion`, `marritial_stat`, `mobile`, `job_status`, `Nationality`, `interest`, `bio`, `nid`, `passpoet`, `skill_arel`, `languages`, `profile_pic`, `blood`, `height`, `fax`, `address`, `other`, `created`, `modified`, `deleted`, `full_name`) VALUES
(1, 60, 'father', 'mother', 'gender', 'birthday', 'occupation', 'education', 'religion', 'married', 'mobile', 'job', 'nation', 'interest', 'bio', 9999, 7777, 'skill', 'language', 0x322e35363934333537393532383430373135653230, 'blood', 'height', 'fax', 'address', 'other', '2016-08-11 00:00:00', '2016-08-27 00:00:00', '2016-08-25 00:00:00', 'Mysql Hitachi'),
(2, 66, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(3, 67, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '2016-08-06 09:48:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(4, 68, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '2016-08-06 10:25:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
