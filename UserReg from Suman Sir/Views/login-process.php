<?php
include "../Src/users/users.php";
use App\Users\Users;

$obj = new users();
//echo "<pre>";
//print_r($alldata);
//die();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['uname'])) {
        if (!empty($_POST['pw1'])) {
            $obj->prepare($_POST)->login();
        } else {
            $_SESSION['Message'] = "Enter password";
            header('location:login.php');
        }
    } else {
        $_SESSION['Message'] = "Enter username";
        header('location:login.php');
    }
} else {
    $_SESSION['Message'] = "Opps something going wrong!";
    header('location:login.php');
}