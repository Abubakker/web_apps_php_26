<?php
include_once("../vendor/autoload.php");
use App\users\users;
$obj = new users();
include("include/header.php");
?>

<!-- Registration form - START -->
<div class="container">
    <div class="row">
        <form role="form" method="post" action="login-process.php">
            <div class="col-lg-6">
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required
                        Field</strong></div>
                <div class="form-group">
                    <label for="InputName">Username*</label>

                    <div class="input-group">
                        <input type="text" name="uname" class="form-control" name="InputName" id="InputName"
                               placeholder="Username" >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="InputName">Password*</label>

                    <div class="input-group">
                        <input type="password" name="pw1" class="form-control" name="InputName" id="InputName"
                               placeholder="Password" >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>

                <input type="submit" name="submit" id="submit" value="Login" class="btn btn-info pull-right">
            </div>
        </form>
        <div class="col-lg-5 col-md-push-1">
            <div class="col-md-12">
                <?php if (isset($_SESSION['Message'])) { ?>
                    <div class="alert alert-success">
                        <strong><span class=""></span>
                            <?php
                            echo $_SESSION['Message'];
                            unset($_SESSION['Message']);
                            ?>
                        </strong>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<!-- Registration form - END -->

</div>

</body>
</html>