<?php
    $handle = fopen("contacts.csv","r");
    $data = fgetcsv($handle, 1000, ",");
    $color = $data[3];
    $options = $data[5];
    echo('<table>');
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        //generate HTML
        echo('<tr data-color="' . $color . '" data-options="' . $options . '">');
        foreach ($data as $index=>$val) {
            echo('<td>');
            echo htmlentities($val, ENT_QUOTES);
            echo('</td>');
        }
        echo('</tr>');
    }
    echo("</table>");
    fclose($handle);
?>